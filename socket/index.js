import * as config from "./config";
import gameRooms from "./gameRooms";

const rooms = ["ROOM_1", "ROOM_2", "ROOM_3"];



export default io => {
  gameRooms(io.of("/game"));
};

