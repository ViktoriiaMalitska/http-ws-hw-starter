import Users from "./users.mjs";
import {SECONDS_TIMER_BEFORE_START_GAME} from "./config";
let users = new Users();
let roomsWithTextId = new Map();
const rooms = ["ROOM_1", "ROOM_2", "ROOM_3"];
function randomInteger(min, max) {
  // случайное число от min до (max+1)
  let rand = min + Math.random() * (max + 1 - min);
  return Math.floor(rand);
}

export default (io) => {
  io.on("connection", (socket) => {
    const username = socket.handshake.query.username;
    let usernameLowercase = username.toLowerCase();
    let userExist = users.getByName(usernameLowercase);

    if (!userExist) {
      users.addUser({
        name: usernameLowercase,
        userId: socket.id,
        roomId: null,
        ready: false,
      });
      socket.emit("UPDATE_USERS_TOTAL", "user aded");
    } else {
      socket.emit("CLEAR_STORAGE_NAME");
    }
    socket.on("disconnecting", () => {
      let discUser =  users.remove(socket.id);
      if(discUser) {
      let activeRoom = discUser.roomId;
      if(activeRoom) {
        const usersInRoom = users.getAllInRoom(activeRoom);
        console.log(`users in room ${usersInRoom}`)
        socket.broadcast.to(activeRoom).emit("UPDATE_USERS", usersInRoom);
        socket.emit("EXIT_ROOM_DONE");
      }
    }
    });

    socket.on("JOIN_ROOM", (user) => {
      let { userId, roomId, ready } = user;
      let userObj = users.get(userId);
      if (userObj.roomId === roomId) {
        return;
      }
      socket.leave(userObj.roomId);
      userObj.roomId = roomId;
      socket.join(roomId);
      let usersInRoom = users.getAllInRoom(roomId);
      socket.broadcast.to(roomId).emit("UPDATE_USERS", usersInRoom);
      // socket.join(roomId, () => {
      //   io.to(socket.id).emit("NEW_USER-IN_ROOM", userObj);
      // });
      const roomsUsers = users.getAllInRoom(roomId);

      socket.emit("JOIN_ROOM_DONE", { roomId: roomId, roomsUsers: roomsUsers });
    });

    socket.on("READY_TO_PLAY", (id) => {
      let userObj = users.get(id);
      userObj.ready = !userObj.ready;
      const usersInRoom = users.getAllInRoom(userObj.roomId);
      socket.broadcast.to(userObj.roomId).emit("UPDATE_USERS", usersInRoom);
      socket.emit("UPDATE_USERS", usersInRoom);
       if(users.checkOnReady(userObj.roomId)) {
        socket.broadcast.to(userObj.roomId).emit("START_TIMER_BEFORE");
        socket.emit("START_TIMER_BEFORE");
       }
    });

    socket.on("START_TIMER_BEFORE_DONE", (userId) => {
      let roomId = users.get(userId).roomId;
      let counter = 0;
      let remaining;
      let interval = setInterval(function() {
        remaining = SECONDS_TIMER_BEFORE_START_GAME - counter;
        socket.broadcast.to(roomId).emit('COUNT_DOUN', remaining);
        socket.emit("COUNT_DOUN", remaining);
        if (counter >= SECONDS_TIMER_BEFORE_START_GAME) {
          // socket.emit('START_GAME', counter);
          clearInterval(interval);
          let textId = randomInteger(0,6);
          if(!roomsWithTextId.get(roomId)) {
            roomsWithTextId.set(roomId, textId);
          }
          let textIdForHTTP = roomsWithTextId.get(roomId);
          io.to(socket.id).emit("GET_TEXT_ID", textIdForHTTP);  
        }
        counter += 1;
      }, 1000);
    })
    
    socket.on("EXIT_ROOM", (id) => {
      let userObj = users.get(id);
      let previosRoomId = userObj.roomId;
      if (userObj) {
        socket.leave(id);
        socket.leave(users.roomId, () => {
          io.to(socket.id).emit("JOIN_ROOM_DONE", users.roomId);
        });
        userObj.roomId = null;
      }
      const usersInRoom = users.getAllInRoom(previosRoomId);
      socket.broadcast.to(previosRoomId).emit("UPDATE_USERS", usersInRoom);
      socket.emit("EXIT_ROOM_DONE", id);
    });
    socket.emit("UPDATE_ROOMS", rooms);
  });
};
