class Users {
  constructor() {
    this.users = [];
  }
  addUser(user) {
    this.users.push(user);
  }
  get(id) {
    return this.users.find((user) => user.userId === id);
  }

  getByName(name) {
    return this.users.find((user) => user.name === name);
  }
  setProperty(id, key, value) {
    let d = this.get(id);
    d[key] = value;
  }
  remove(id) {
    const user = this.get(id);
    if (user) {
      this.users = this.users.filter((user) => user.userId !== id);
    }
    return user;
  }
  checkOnReady(room) {
    let userInRoom = this.users.filter((user) => user.roomId === room);
    const arr = userInRoom.filter((user) => user.ready == false);
    if (arr.length === 0) {
      return true;
    }
    return false;
  }

  getAllInRoom(room) {
    return this.users.filter((user) => user.roomId === room);
  }
}

export default Users;
