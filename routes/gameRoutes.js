import { Router } from "express";
import path from "path";
import { HTML_FILES_PATH } from "../config";
// import { getText } from "./servise/getText";
import texts from "../data.js";

const router = Router();

router
  .get("/", (req, res) => {
    const page = path.join(HTML_FILES_PATH, "game.html");
    res.sendFile(page);
  })
    .get("/texts/:id",(req, res) => {
      const item = req.params.id;
      const textForGame = texts.texts[item];
      res.send(textForGame)
      // try {
      //   const item = ctx.params.id

      //     console.log(texts.texts[item])
      //     ctx.body = {
      //         success: true,
      //         result: texts[item]
      //     };
      // } catch (err) {
      //     ctx.body = {
      //         success: false,
      //         result: err
      //     };
      // }
  }); 

export default router;
