import { createElement, addClass, removeClass } from "./helper.mjs";
// import { response } from "express";
// import { startGame } from "./startGame.mjs";
let activeRoomId = null;
const setActiveRoomId = (roomId) => {
  activeRoomId = roomId;
};

const gameContainer = document.getElementById("game-page");
const roomsContainer = document.getElementById("rooms-page");
const roomsItemConteiner = document.getElementById("rooms-container");
const nameRoomEl = document.getElementById("room-name");
const backBut = document.getElementById("returnBut");
const username = sessionStorage.getItem("username");
const userDisplay = document.getElementById("users-display");
const onReady = document.getElementById("on-ready");
const counter = document.getElementById("counter-container");
const counterValue = document.getElementById("counter-value");
const textContainer = document.getElementById("text-container");
if (!username) {
  window.location.replace("/login");
}
const socket = io("http://localhost:3002/game", { query: { username } });

const createUsersItem = (user) => {
  const { name, userId, roomId, ready } = user;
  
  
  const userItem = createElement({
    tagName: "div",
    className: "user-item",
    attributes: { id: userId },
  });

  const nameUser = createElement({
    tagName: "label",
    className: "name-item",
    attributes: {
      id: "progres-item-label",
      for: "progres-item",
      value: `${name}`,
    },
  });
  nameUser.innerHTML = `${name}`;
  const progresItem = createElement({
    tagName: "progress",
    className: "progres-item",
    attributes: { id: "progres-item", max: "100", value: "0" },
  });
  if(userId === socket.id) {
    const aut = document.createTextNode("You");
    nameUser.append(aut);
  }
  let letsPlay = ready ? "letsPlay" : "noPlay";
  const readyCicle = createElement({
    tagName: "div",
    className: "ready",
    attributes: { id: "ready", class: letsPlay },
  });
  
  userItem.append(nameUser);
  userItem.append(progresItem);
  userItem.append(readyCicle);
  userDisplay.append(userItem);
  return;
};

const createRoomBox = (roomId) => {
  const onJoinRoom = () => {
    if (activeRoomId === roomId) {
      return;
    }
    socket.emit("JOIN_ROOM", {
      userId: socket.id,
      roomId: roomId,
      ready: false,
    });
  };
  const exitRoom = () => {
    if (activeRoomId === roomId) {
      return;
    }
    activeRoomId = null;
    socket.emit("EXIT_ROOM", socket.id);
  };

  const swichReady = () => {

    if(onReady.innerHTML === "Ready" ? onReady.innerHTML = `NOT READY` : onReady.innerHTML = `Ready` )
    
    socket.emit("READY_TO_PLAY", socket.id);
  };

  const roomDiv = createElement({
    tagName: "div",
    className: "room-item flex-centered no-select",
    attributes: { id: roomId },
  });
  const connectedUsers = createElement({
    tagName: "div",
    className: "connectedUsers",
  });
  let text = document.createTextNode(`users connected`);
  connectedUsers.append(text);

  const roomName = createElement({
    tagName: "div",
    className: "roomName",
  });
  text = document.createTextNode(`Room name ${roomId}`);
  roomName.append(text);

  text.innerHTML = `Room name ${roomId}`;
  const but = createElement({
    tagName: "button",
    className: "roomBut",
  });
  but.innerHTML = "Join";

  but.addEventListener("click", onJoinRoom);
  backBut.addEventListener("click", exitRoom);
  onReady.addEventListener("click", swichReady);

  roomDiv.append(connectedUsers);
  roomDiv.append(roomName);
  roomDiv.append(but);
  return roomDiv;
};

const updateRooms = (rooms) => {
  const allRooms = rooms.map(createRoomBox);
  roomsItemConteiner.append(...allRooms);
};

const joinRoomDone = (data) => {
  let { roomId, roomsUsers } = data;
  userDisplay.innerHTML = ``;
  roomsUsers.forEach((element) => {
    createUsersItem(element);
  });

  addClass(gameContainer, "active");
  if (activeRoomId) {
    const previousRoomElement = document.getElementById(activeRoomId);
    removeClass(previousRoomElement, "active");
  }
  nameRoomEl.innerHTML = `${roomId}`;
  removeClass(gameContainer, "display-none");
  addClass(roomsContainer, "display-none");
  setActiveRoomId(roomId);
};

const updateUsersDisplay = (usersList) => {

  userDisplay.innerHTML = ``;
  usersList.forEach((element) => {
    if(element.userId ==+ socket.id) {

    }
    createUsersItem(element);
  });
};

const exitRoomDone = () => {
  removeClass(gameContainer, "active");
  userDisplay.innerHTML = ``;
  addClass(gameContainer, "display-none");
  removeClass(roomsContainer, "display-none");
  setActiveRoomId(null);
};
const cleareSessionStorage = () => {
  sessionStorage.removeItem("username");
  alert("User already exist!");
  window.location.replace("/login");
};

const disconectButton = () => {
  console.log("START_TIMER_BEFORE")
  addClass(onReady, "display-none");
  addClass(backBut, "display-none");
  removeClass(counter, "display-none");
  socket.emit("START_TIMER_BEFORE_DONE", socket.id);
}

const updateCounterValue = (value = "") => {
  counterValue.innerText = value;
}


const addTextToDisplay = (text) => {
  console.log("add classes")
  addClass(counter, "display-none");
  removeClass(textContainer, "display-none");
  addClass(counterValue, "display-none");
  textContainer.innerText = text
}
const start = (idText) => {
  fetch(`/game/texts/${idText}`)
  .then(response => response.text())
  .then(response => addTextToDisplay(response));

  
}
socket.on("EXIT_ROOM_DONE", exitRoomDone);
socket.on("CLEAR_STORAGE_NAME", cleareSessionStorage);
socket.on("UPDATE_ROOMS", updateRooms);
socket.on("JOIN_ROOM_DONE", joinRoomDone);
socket.on("UPDATE_USERS", updateUsersDisplay);
socket.on("START_TIMER_BEFORE", disconectButton);
socket.on("COUNT_DOUN", updateCounterValue);
socket.on("GET_TEXT_ID", start )
