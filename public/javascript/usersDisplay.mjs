export const createUsersItem = (user) => {
    const {name, userId, roomId} = user;
    const userItem = createElement({
        tagName: "div",
        className: "user-item",
        attributes: { id: userId }
      });
      let text = document.createTextNode(`${name}`);
      userItem.append(text);
      const progresItem = createElement({
        tagName: "div",
        className: "progres-item",
        attributes: { id: userId }
      });
      const ready = createElement({
        tagName: "div",
        className: "ready",
        attributes: { id: userId }
      });
      let text = document.createTextNode(`${name}`);
      userItem.append(text);
      progresItem.append(text);
      ready.append(text);

      return userItem;
}