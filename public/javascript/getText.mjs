const getTextFromData = (idText) =>
    fetch(`/game/texts/${idText}`, {
        method: "GET"
    });

    export async function getText() {
        const result = await getTextFromData();
        const res = await result.json();
        const list = res.result;
        return(list)
    }